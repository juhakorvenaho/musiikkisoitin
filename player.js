'use strict';

// luodaan musiikkikirjasto arrayna ja alustetaan soitin
var library = new Array('./media/awake10_megaWall.mp3', './media/DST-TowerDefenseTheme.mp3', './media/ThrustSequence.mp3', './media/OrbitalColossus.mp3');
var i = 0;
var player = new Audio(library[i]);
var volume = 100;
var song = i+1;

// kuin sivu ladataan tämä suoritetaan
function onload() {
    currentVolume();
    duration = player.duration;
    //libraryDuration();
    timeUpdate();
}

//koko soittolistan pituus ei toimi enkä ymmärrä miksi. Tuntuu liittyvän siihen että iteroidaan tuo liian nopeasti läpi.
function libraryDuration() {
    var totalDuration = 0;
    var t = 0;
    player.src = library[t];
    while (t < library.length) {
        //yritin tämmöistä funktiota jonka löysin stackoverflowsta, mutta se ei toiminut odotetusti = ei lainkaan
        player.onloadedmetadata = function() {
            player.src = library[t];
            totalDuration += player.duration;
            t++;
        }
    }
    var aika = timeConverter(totalDuration);
    var playlistDuration = (document.getElementById("totalDuration").innerHTML = aika);
    player.src = library[i];   
}

//sekunnit tunneiksi, minuuteiksi ja sekunneiksi
function timeConverter(secs)
{
    var hours = Math.floor(secs / (60 * 60));
    var divideMinutes = secs % (60 * 60);
    var minutes = Math.floor(divideMinutes / 60);
    var divideSeconds = divideMinutes % 60;
    var seconds = Math.ceil(divideSeconds);
    var aika = (hours < 10 ? "0" + hours : hours);
    aika += ":" + (minutes < 10 ? "0" + minutes : minutes);
    aika += ":" + (seconds < 10 ? "0" + seconds : seconds);
    return aika;
}

//kun playta on painettu
function playMusic() {
    if (player.paused) {
        player.play();
        songPlaying();
    } else {
        player.pause();
    }
}

//kun stop on painettu
function stopMusic() {
    player.pause();
    player.src = library[i];
    timeUpdate();
}

//kun next on painettu
function nextSong() {
    if (i == library.length - 1) {
        i = 0;
        song = 1;
    } else {
        i++;
        song++;
    }
    player.pause();
    player.src = library[i];
    player.play();
    songPlaying();
}

//kun previous on painettu
function previousSong() {
    if (i == 0) {
        i = library.length - 1;
        song = library.length;
    } else {
        i--;
        song--;
    }
    player.pause();
    player.src = library[i];
    player.play();
    songPlaying();
}

//kelaus taakse
function backwards() {
    if (player.currentTime - 10 < 0) {
        previousSong();
    }else{
        player.currentTime -= 10;
    }
    
}

//kelaus eteen
function forwards() {
    if (player.currentTime + 10 > player.duration) {
        nextSong();
    }else{
        player.currentTime += 10;
    }
    
}

//kun voldown painettu
function volumeDown() {
    if (volume == 10) {
        currentVolume();
        player.volume = 0.1;
    } else {
        player.volume -= 0.1;
        volume -= 10;
        currentVolume();
    }
}

//kun volup on painettu
function volumeUp() {
    if (volume == 100) {
        currentVolume();
    } else {
        player.volume += 0.1;
        volume += 10;
        currentVolume();
    }
}

//näyttää nykyisen äänenvoimakkuuden tason
function currentVolume() {
    var currentVolume = (document.getElementById("volume").innerHTML = volume + "%");
}

function songPlaying() {
    var allSongs = library.length;
    var songPlaying = (document.getElementById("song").innerHTML = song + " of " + allSongs);
}

//muuttujat kappaleen aikanäyttöön
var duration;
var music = document.getElementById('playhead');
var timePlaying = document.getElementById("duration");

//kuunnellaan muutoksia ja kutsutaan aikaa päivittävää funktiota sitämukaa kun kappale etenee
player.addEventListener("timeupdate", timeUpdate, false);

//kun kappale soi on päivitettävä kohtaa missä ollaan menossa
function timeUpdate() {
    var playPercent = 100 * (player.currentTime / duration);
    playhead.style.marginLeft = playPercent + "%";
    var currentTime = timeConverter(player.currentTime)
    var timeLeft = timeConverter(duration-player.currentTime);
    var total = timeConverter(duration);
    timePlaying = (document.getElementById("duration").innerHTML = currentTime + " / " + timeLeft + " / Total: " + total);
    if (playPercent == 100) {
        nextSong();
    }
}

//tutkitaan kuinka pitkä kappale on kyseessä
player.addEventListener("canplaythrough", function (e) {
    duration = player.duration;
}, false);